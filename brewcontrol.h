/* Copyright (c) 2017 Philippe Kalaf, MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, 
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or 
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef brewcontrol_h
#define brewcontrol_h

#include "mbed.h"
#include "fhksc.h"
#include "phasecontrol.h"
#include "lmt01.h"
#include "pidcontrol.h"
#include "pressuresensor.h"

#define MODE_TIME 1
#define MODE_VOLUME 2
#define MODE_FLOW_RATE 3
#define MODE_MANUAL 4
#define MODE_PRE_INFUSE 5

// Brew states
#define STOPPED 0
#define BREWING 1
#define PRE_INFUSING 2
#define SOFT_STOPPING 3

#define PORTAFILTER_VOLUME 100

//#define TEMP2

/// This is the main class where all brew functions and sensors are handled
class BrewControl 
{
public:
    /// Constructor requires the pin numbers for all inputs and outputs
    BrewControl(PinName brew_pin, 
                PinName flow_sensor_pin, 
                PinName zcd_input_pin,
                PinName pump_control_pin,
                PinName pressure_sensor_pin,
                PinName temp_sensor_pin,
#ifdef TEMP2
		PinName temp2_sensor_pin,
#endif
                PinName boiler_pwm_pin);

    /// Set the target shot temperature in C
    void set_shot_temperature(float temperature);
    /// Get the target shot temperature in C
    float get_shot_temperature();
    /// Get the live temperature of water in the boiler in C
    float get_current_temperature();
    float get_current_temperature_side();
    float get_current_temperature_top();

    /// Set the target shot time in s
    void set_shot_time(int time);
    /// Get the target shot time in s
    int get_shot_time();
    /// Get the live shot clock time in s
    float get_current_time();

    /// Set the required pre-infuse time in s, 0 for no pre-infuse
    void set_preinfuse_time(int time);
    /// Get the current set pre-infuse time in s, 0 if no pre-infuse is set
    int get_preinfuse_time();
    /// Function call to manually set pre-infuse quantity
    void stop_preinfuse_now();

    /// Set the target shot volume in ml (default 60ml)
    void set_shot_volume(int volume);
    /// Get the target shot volume in ml
    int get_shot_volume();
    /// Get the live shot volume in ml
    float get_current_volume();

    /// Set the target shot flow rate in ml/s
    void set_shot_flow_rate(float flow_rate);
    /// Get the target shot flow rate in ml/s 
    float get_shot_flow_rate();
    /// Get the live shot flow rate in ml/s
    float get_current_flow_rate();
    
    
    /// Set the target pressure in bars 
    void set_shot_pressure(float pressure);
    /// Get the target pressure in bars
    float get_shot_pressure();
    /// Get the current measured pressure in bars
    float get_current_pressure();
    void pressure_up(uint8_t value = 1);
    void pressure_down(uint8_t value = 1);
    /// Get the currently set pump level (0 to 100%)
    uint8_t get_pump_level();

    /// Return 1 if currently brewing, 0 if not
    uint8_t get_state();
    /// Toggle brewing
    uint8_t toggle();
    /// Start brewing, this resets all live variables and based on set targets initiates the right brew mode, returns set mode
    uint8_t start();
    /// Stop brewing with reduced backflush
    void soft_stop();

    /// Enable PID boiler control
    void enable_boiler();
    /// Disable PID boiler control, this means water remains cold
    void disable_boiler();
    /// Toggle PID boiler control
    bool toggle_boiler();

    /// Get average pressure of last shot
    float get_average_pressure();

    PhaseControl *get_pump_control_ptr();
    void toggle_solenoid();
    uint16_t get_last_pulse_count_side();
    uint16_t get_last_pulse_count_top();

private:
    // Signal to turn on/off pump
    DigitalOut _brew_switch;
    
    // Flow sensor
    FHKSC _flow_sensor;
    
    // Pump phase controller for pressure control
    PhaseControl _pump_control;

    // Soft stop reduces backflush
    Timeout _soft_stop_timer;
    
    // Pressure Sensor
    PressureSensor _pressure_sensor;
    
    // Temperature control
    LMT01 _temp_sensor;
#ifdef TEMP2
    LMT01 _temp2_sensor;
#endif
    PwmOut _boiler_pwm;
    PIDControl _boiler_pid;
    float _latest_temp;
    
    // Boiler state
    bool _enable_boiler;
    
    // The shot clock is the time spent brewing
    float _shot_clock;

    // Desired shot time, volume and temperature during a brew
    int _target_shot_time;
    int _target_shot_volume;
    float _target_shot_temperature;
    float _target_flow_rate;
    float _target_shot_pressure;

    // Operating modes
    uint8_t _state;
    int _mode;

    // The average pressure of previous shot
    float _average_pressure;
    
    // These are used for pre-infuse mode to go back to initial settings after
    // pre-infuse is done
    int _prev_mode;
    int _prev_pressure;
    int _preinfuse_time;

    // This is for manual pre-infuse control
    bool _stop_preinfuse;
    
    // Workers
    Ticker _brew_ticker;
    void _brew_worker();
    Ticker _boiler_pid_ticker;
    void _boiler_pid_worker();
    void _stop();
};

#endif// brewcontrol_h
