/* Copyright (c) 2017 Philippe Kalaf, MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, 
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or 
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


// brew worker to run 10 times per second
#define BREW_WORKER_PERIOD 0.1

// Boiler PID worker to run 1 times per second
#define PID_WORKER_PERIOD 1

// PWM period
// 0.8333 for 60Hz, 1 for 50Hz for 1% resolution
#define BOILER_PWM_PERIOD 0.8333

// Soft stop time
#define SOFT_STOP_TIME_S 7.0

// Manage different brew modes and timings
#include "brewcontrol.h"

BrewControl::BrewControl(   PinName brew_pin, 
                            PinName flow_sensor_pin,
                            PinName zcd_input_pin,
                            PinName pump_control_pin,
                            PinName pressure_sensor_pin,
                            PinName temp_sensor_pin,
#ifdef TEMP2
			    PinName temp2_sensor_pin,
#endif
                            PinName boiler_pwm_pin
                            ) : 
                            _brew_switch(brew_pin, 0), 
                            _flow_sensor(flow_sensor_pin), 
                            _pump_control(zcd_input_pin, pump_control_pin),
                            _pressure_sensor(pressure_sensor_pin),
                            _temp_sensor(temp_sensor_pin),
#ifdef TEMP2
			    _temp2_sensor(temp2_sensor_pin),
#endif
                            _boiler_pwm(boiler_pwm_pin)
{
    _preinfuse_time = 0;
    _brew_switch = 0;
    
    // at 60Hz, we got 120 zero-crosses per sec, we want to capture 100 of 
    // those within each PWM period
    _boiler_pwm.period(BOILER_PWM_PERIOD);
    
    // let's start at 93 C
    _target_shot_temperature = 93;

    // 9 bars is default
    _target_shot_pressure = 9;

    _boiler_pid.setPIDGains(0.075, 0.1, 0.9);
    _boiler_pid.setIntegratorLimits(0, 1);

    // Boiler is on by default
    enable_boiler();
}

float BrewControl::get_current_temperature_side()
{
    return _temp_sensor.read();
}

float BrewControl::get_current_temperature_top()
{
#ifdef TEMP2
    return _temp2_sensor.read();
#endif
    return 0;
}

float BrewControl::get_current_temperature()
{
    //return (_temp_sensor.read() + _temp2_sensor.read())/2;
    return get_current_temperature_side();
}

void BrewControl::set_shot_temperature(float shot_temp)
{
    if (shot_temp <= 20)
        _target_shot_temperature = 20;
    else if (shot_temp >= 150)
        _target_shot_temperature = 150;
    else
        _target_shot_temperature = shot_temp;
}

float BrewControl::get_shot_temperature()
{
    return _target_shot_temperature;
}

void BrewControl::set_shot_pressure(float pressure)
{
    if(pressure <= 0)
	_target_shot_pressure = 0;
    else if(pressure >= 12)
	_target_shot_pressure = 12;
    _target_shot_pressure = pressure;
}

// Return pressure in bars
float BrewControl::get_current_pressure()
{
    return _pressure_sensor.read_bars();
}

int BrewControl::get_shot_volume()
{
    return _target_shot_volume;
}


void BrewControl::_boiler_pid_worker()
{
    // take temperature measurement
    float latestTemp = get_current_temperature(); 
        
    float power = 0.0;

    // if the temperature is near zero, we assume there's an error
    if ( latestTemp > 0.5 ) {
        // calculate PID update
        power = _boiler_pid.update(_target_shot_temperature - latestTemp,
                                    latestTemp);
    }

    // Validate number
    if ( power > 1 )
        power = 1;
    else if ( power < 0 )
        power = 0;

    // let's set new power on boiler
    _boiler_pwm = power;

    // store the latest temperature reading
    _latest_temp = latestTemp;
}

void BrewControl::enable_boiler()
{
    _enable_boiler = 1;
    _boiler_pid_ticker.attach(callback(this, &BrewControl::_boiler_pid_worker),
                                PID_WORKER_PERIOD);
}

void BrewControl::disable_boiler()
{
    _enable_boiler = 0;
    _boiler_pid_ticker.detach();
    _boiler_pwm = 0;
}

bool BrewControl::toggle_boiler()
{
    if (_enable_boiler)
        disable_boiler();
    else
        enable_boiler();
    
    return _enable_boiler;
}

void BrewControl::pressure_up(uint8_t value)
{
    _pump_control.level_up(value);
}

void BrewControl::pressure_down(uint8_t value)
{
    _pump_control.level_down(value);
}

// pre-infuse time in seconds
// set to 0 to disable
void BrewControl::set_preinfuse_time(int time)
{
    if (time > 0)
        _preinfuse_time = time;
    else
        _preinfuse_time = 0;
}

int BrewControl::get_preinfuse_time()
{
    return _preinfuse_time;
}

void BrewControl::stop_preinfuse_now()
{
    _stop_preinfuse = 1;
}

uint8_t BrewControl::get_pump_level()
{
    return _pump_control.get_level();
}

// This is to set the wanted shot time
void BrewControl::set_shot_time(int time)
{
    _target_shot_time = time;
}

// This is to set the wanted shot volume
void BrewControl::set_shot_volume(int volume)
{
    if (volume >= 0)
        _target_shot_volume = volume;
}

// This is to set the wanted flow rate (ml/s)
void BrewControl::set_shot_flow_rate(float flow_rate)
{
    _target_flow_rate = flow_rate;
}

// return current shot_clock in seconds
float BrewControl::get_current_time()
{
    return _shot_clock;
}

// return current volume in ml
float BrewControl::get_current_volume()
{
    return _flow_sensor.read();
}

// return the current flow rate
float BrewControl::get_current_flow_rate()
{
    return _flow_sensor.get_flow_rate();
}

// read brew on/off state
uint8_t BrewControl::get_state()
{
    return _state;
}

void BrewControl::_brew_worker()
{
    _shot_clock = _shot_clock + BREW_WORKER_PERIOD;

    if (_mode == MODE_PRE_INFUSE)
    {
        // First let's fill up the portafilter
        if (_flow_sensor.read() <= PORTAFILTER_VOLUME && !_stop_preinfuse)
            return;
        
        // It's full, let's stop the pump for set pre-infuse time
        if (_pump_control.get_level() != 0)
        {
            _pump_control.set_level(0);
            _shot_clock = 0;
        }
        
        // Once pre-infuse time runs out, set brew mode
        if (_shot_clock >= _preinfuse_time)
        {
            _mode = _prev_mode;
            _pump_control.set_level(_prev_pressure);
            _shot_clock = 0;
            _flow_sensor.reset_count();
	    _state = BREWING;
	    _stop_preinfuse = 0;
	    _pressure_sensor.start_count();
        }
    }
    else if(_mode == MODE_TIME)
    {
	// Auto-adjust pressure to target
	float error = _target_shot_pressure - get_current_pressure();
	if(error < -0.25)
	    pressure_down();
	else if(error > 0.25) 
	    pressure_up();

        if(_shot_clock >= _target_shot_time)
            soft_stop();
    }
    else if(_mode == MODE_VOLUME)
    {
	// Auto-adjust pressure to target
	float error = _target_shot_pressure - get_current_pressure();
        if(error < -0.25)
	    pressure_down();
	else if(error > 0.25)
	    pressure_up();

	if(_flow_sensor.read() >= _target_shot_volume)
            soft_stop();
    }
    else if(_mode == MODE_FLOW_RATE)
    {
	// Re-calculate target flowrate
	_target_flow_rate = (_target_shot_volume - _flow_sensor.read())
	    / (_target_shot_time - _shot_clock);
	// Auto-adjust flow-rate
	if(_target_flow_rate - _flow_sensor.get_flow_rate() < 0)
	    pressure_down();
	else
	    pressure_up();

	// Stop when target shot volume is reached
	if(_flow_sensor.read() >= _target_shot_volume)
            soft_stop();
    }
    else if(_mode == MODE_MANUAL)
    {
    }
}

uint8_t BrewControl::start()
{
    if(_state) return _mode;
    
    _state = BREWING;
    
    // reset shot clock and flow sensor
    _flow_sensor.reset_count();
    _shot_clock = 0;
    _brew_ticker.detach();
    
    // We have 4 operating modes based on which values are set or unset
    
    // Time mode - constant pressure, constant time, variable volume
    if (_target_shot_time && !_target_shot_volume)
        _mode = MODE_TIME;
        
    // Volume mode - constant pressure, constant volume, variable time
    else if(!_target_shot_time && _target_shot_volume)
        _mode = MODE_VOLUME;
        
    // Flowrate mode - constant volume, constant time, variable flow-rate 
    else if(_target_shot_time && _target_shot_volume)
        _mode = MODE_FLOW_RATE;
        
    // Manual on/off via start(), stop(), toggle()
    else
        _mode = MODE_MANUAL;
    
    // Let's save settings before we set pre-infuse mode
    if (_preinfuse_time && _mode != MODE_MANUAL)
    {
        _prev_mode = _mode;
        _prev_pressure = get_pump_level();
	// set pre-infuse mode
        _mode = MODE_PRE_INFUSE;
	_state = PRE_INFUSING;

	// we pre-infuse at low pressure
        _pump_control.set_level(45);
    }
    
    // Turn on brewing circuits
    _brew_switch = 1;
    
    // Run worker at defined period
    if(_mode != MODE_MANUAL)
	    _brew_ticker.attach(callback(this, &BrewControl::_brew_worker),
			    BREW_WORKER_PERIOD);

    return _mode;
}

void BrewControl::toggle_solenoid()
{
    if(_brew_switch)
	_brew_switch = 0;
    else
	_brew_switch = 1;
}

void BrewControl::_stop()
{
    _state = STOPPED;
    _brew_switch = 0;
    _shot_clock = 0;
    set_shot_volume(0);
    set_shot_time(0);
    _flow_sensor.reset_count();
}

void BrewControl::soft_stop()
{
    _pump_control.set_level(0);
    _brew_ticker.detach();
    _soft_stop_timer.attach(callback(this, &BrewControl::_stop),
			SOFT_STOP_TIME_S);
    _average_pressure = _pressure_sensor.stop_count();
    _state = SOFT_STOPPING;
}

float BrewControl::get_average_pressure()
{
    return _average_pressure;
}

uint8_t BrewControl::toggle()
{
    if(_state == BREWING || _state == PRE_INFUSING)
        soft_stop();
    else if(_state == STOPPED)
        start();
        
    return _state;
}

PhaseControl *BrewControl::get_pump_control_ptr()
{
    return &_pump_control;
}

uint16_t BrewControl::get_last_pulse_count_side()
{
    return _temp_sensor.get_last_pulse_count();
}

uint16_t BrewControl::get_last_pulse_count_top()
{
#ifdef TEMP2
    return _temp2_sensor.get_last_pulse_count();
#endif
}
